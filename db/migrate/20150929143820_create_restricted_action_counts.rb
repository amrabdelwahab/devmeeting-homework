class CreateRestrictedActionCounts < ActiveRecord::Migration
  def change
    create_table :restricted_action_counts do |t|
      t.integer :count, :default => 0
      t.integer :user_id

      t.timestamps
    end
  end
end
