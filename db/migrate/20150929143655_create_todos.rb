class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.string :text
      t.boolean :checked, default: false
      t.integer :user_id
      t.timestamps
    end
  end
end
