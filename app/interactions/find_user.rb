require 'active_interaction'
# interaction for register
class FindUser < ActiveInteraction::Base
  integer :user_id
  validates :user_id, presence: true

  def execute
    user = User.find_by_id(user_id)
    if user
      user
    else
      errors.add(:user_id, 'does not exist')
    end
  end
end
