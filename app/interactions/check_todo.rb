require 'active_interaction'
# interaction for register
class CheckTodo < ActiveInteraction::Base
  integer :user_id, :todo_id
  validates :user_id, presence: true
  validates :todo_id, presence: true

  def execute
    todo = FindTodo.run(user_id: user_id, todo_id: todo_id)
    return errors.merge!(todo.errors) unless todo.valid?
    user = Restrict.run(user_id: user_id)
    return errors.merge!(user.errors) unless user.valid?
    todo.result.checked = true
    todo.result.save
    return { Warnings: user.result, todo: todo.result } if user.result.present?
    todo.result
  end
end
