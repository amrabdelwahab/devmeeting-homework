require 'active_interaction'
# interaction for register
class Confirm < ActiveInteraction::Base
  integer :user_id
  validates :user_id, presence: true

  def execute
    user = FindUser.run(user_id: user_id)
    return errors.merge!(user.errors) unless user.valid?
    user.result.confirmed_at = DateTime.now
    user.result.save
    user.result
  end
end
