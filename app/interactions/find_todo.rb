require 'active_interaction'
# interaction for register
class FindTodo < ActiveInteraction::Base
  integer :user_id, :todo_id
  validates :user_id, presence: true
  validates :todo_id, presence: true

  def execute
    todo = Todo.find_by(user_id: user_id, id: todo_id)
    if todo
      todo
    else
      errors.add(:todo, 'does not exist')
    end
  end
end
