require 'active_interaction'
# interaction for register
class Restrict < ActiveInteraction::Base
  integer :user_id
  validates :user_id, presence: true

  def execute
    user = FindUser.run(user_id: user_id)
    return errors.merge!(user.errors) unless user.valid?
    return nil unless user.result.confirmed_at.nil?
    count = Count.run(user_id: user_id)
    return errors.merge!(count.errors) unless count.valid?
    count.result
  end
end
