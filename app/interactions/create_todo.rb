require 'active_interaction'
# interaction for register
class CreateTodo < ActiveInteraction::Base
  string :text
  integer :user_id
  validates :user_id, presence: true

  def execute
    user = Restrict.run(user_id: user_id)
    return errors.merge!(user.errors) unless user.valid?
    todo = Todo.new(inputs)
    errors.merge!(todo.errors) unless todo.save
    return { Warnings: user.result, todo: todo } if user.result.present?
    todo
  end
end
