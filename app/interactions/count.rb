require 'active_interaction'
# interaction for register
class Count< ActiveInteraction::Base
  integer :user_id
  validates :user_id, presence: true

  def execute
    count = RestrictedActionCount.find_or_create_by(user_id: user_id)
    count.count += 1
    count.save
    errors.add(:user, 'Please confirm') unless count.count < 4
    "You have just #{ 3 -  count.count } restricted actions left"
  end
end
