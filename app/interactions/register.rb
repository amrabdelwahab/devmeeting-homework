require 'active_interaction'
# interaction for register
class Register < ActiveInteraction::Base
  string :username
  validates :username, presence: true

  def execute
    user = User.new(inputs)
    errors.merge!(user.errors) unless user.save
    user
  end
end
