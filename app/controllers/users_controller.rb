class UsersController < ApplicationController
  protect_from_forgery with: :null_session

  # POST /users
  # POST /users.json
  def create
    @user = Register.run(user_params)

    respond_to do |format|
      if @user.valid?
        format.json { render json: @user.result.to_json, status: :created }
      else
        format.json { render json: @user.errors.messages, status: :unprocessable_entity }
      end
    end
  end

# POST /confirm.json
  def confirm
    @user = Confirm.run({user_id: params[:user_id]})

    respond_to do |format|
      if @user.valid?
        format.json { render json: @user.result.to_json, status: :ok }
      else
        format.json { render json: @user.errors.messages, status: :unprocessable_entity }
      end
    end
  end


  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :confirmed_at)
    end

end
