class TodosController < ApplicationController
protect_from_forgery with: :null_session

  # POST /todos.json
  def create
    @todo = CreateTodo.run(todo_params)

    respond_to do |format|
      if @todo.valid?
        format.json { render json: @todo.result.to_json, status: :created }
      else
        format.json { render json: @todo.errors.messages, status: :unprocessable_entity }
      end
    end
  end

  # POST /check.json
  def check
    @todo = CheckTodo.run(user_id: params[:user_id], todo_id: params[:todo_id])

    respond_to do |format|
      if @todo.valid?
        format.json { render json: @todo.result.to_json, status: :ok }
      else
        format.json { render json: @todo.errors.messages, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_params
      params.require(:todo).permit(:text, :checked, :user_id)
    end
end
