json.array!(@users) do |user|
  json.extract! user, :id, :username, :confirmed_at
  json.url user_url(user, format: :json)
end
